#log 2018-12-31
- Added Ajax Error Handler
- Added Ajax Example
- set Csrf token mode to PersistentTokenMode(true);

#log 2018-12-30
- Added Slim/Csrf
- need to run Composer install
- Added CsrfMiddleware
- Added Ajax Handler

#log 2018-12-29
- Added AuthMiddleware
- Completed User Registration and login
- Added UniqEmail Validation

#log 2018-12-28
- Added AuthController.php
- Added User Model
- Added slim/flash
- Added Auth class in model\auth
- Completed flash.twig

- Added Respect/Validation
- Need to run composer install
- Added Validation Class
- Added User Registration Validation Script
- Added OldInputMiddleware


#log 2018-12-27
- Added flash.twig in to default.twig
- Added alert data in to flash.twig
- Added illuminate/database
- Added robmorgan/phinx
- need to run composer install
- Added 'DEBUG' variable to env.yaml
- Added phinx settings to root dir
- Added login page to home router