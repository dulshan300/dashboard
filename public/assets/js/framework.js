NProgress.configure({
    minimum: 0.01,
    showSpinner: false,
    trickleSpeed: 300
});
var base_url = $('#base_url').val();
// dash board  form on-data handler
$('form[data-on]').each(function(index, el) {
    $(el).submit(function(event) {
        event.preventDefault();
        NProgress.start();
        var controller = $(this).attr('data-on');
        var update = $(this).attr('data-update');
        $(this).append('<input type="hidden" name="controller" value="' + controller + '" /> ');
        var form = $(this).serialize();
        $.post(base_url + '/app/ajax', $(this).serialize()).done(function(data) {
            $(update).html(data);
            NProgress.done();
        }).fail(function(xhr, status, error) {
            NProgress.done();            
            var regex = /:([ a-zA-Z\\:()_.0-9+-]+)/;
            var errors = regex.exec(xhr.responseText)
            bootbox.alert({               
                title: "<b>"+error+"</b>",
                message: '<p style="font-size:1.3em; display:block;padding:5px;" class="bg-danger">'+errors[1]+'</p>',
            });
            // console.log(xhr.responseText);
        });
    });
});