<?php

/*-----------------------
  Authorization Routes
-----------------------*/

$app->get('/', ['App\Controller\Auth\AuthController', 'login'])->setName('auth');
$app->post('/', ['App\Controller\Auth\AuthController', 'postLogin']);

$app->get('/signup', ['App\Controller\Auth\AuthController', 'signup'])->setName('auth.signup');
$app->post('/signup', ['App\Controller\Auth\AuthController', 'postSignup']);

$app->get('/logout', ['App\Controller\Auth\AuthController', 'logout'])->setName('auth.logout');

/*-----------------------
  Application Basic Routes
-----------------------*/

$app->get('/app/install', ['App\Controller\AppController', 'install']);

$app->post('/app/ajax', ['App\Controller\AppController', 'ajaxHandler']);

/*-----------------------
  Dashboard Routes
-----------------------*/

$app->group('/dashboard', function () {
	
	// Your All routers will goes here

    $this->get('', ['App\Controller\DashBoardController', 'index'])->setName('dashboard.home');

})->add(new \App\Middleware\AuthMiddleware($container));

