<?php
use function DI\create;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [

    'settings.debug'               => true,
    'settings.displayErrorDetails' => true,

    // Setting Whoops Error Handler;
    'errorHandler'                 => create(TZeuxisoo\Whoops\Provider\Slim\WhoopsErrorHandler::class),
    'phpErrorHandler'              => create(TZeuxisoo\Whoops\Provider\Slim\WhoopsErrorHandler::class),
];
