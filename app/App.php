<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;

use DI\ContainerBuilder;

class App extends \DI\Bridge\Slim\App{
    
    protected function configureContainer(ContainerBuilder $builder) {
        
        $builder->addDefinitions(__dir__.'/config/app-config.php');
        $builder->addDefinitions(__dir__.'/container.php');       
        
    }
    
    
}