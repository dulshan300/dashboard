<?php
use function DI\get;
use Slim\Flash\Messages;
use Slim\Views\Twig;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


return [
    Twig::class     => function ($c) {
        $twig = new Twig(__DIR__ . '/view', [
            'cache' => false,
            'debug' => true,
        ]);

        $twig->addExtension(new \Slim\Views\TwigExtension(
            $c->get('router'),
            $c->get('request')->getUri()
        ));

        $twig->addExtension(new Twig_Extension_Debug());

        $twig->getEnvironment()->addGlobal('config', $c->get('config'));

        $twig->getEnvironment()->addGlobal('flash', $c->get(Messages::class));

        return $twig;
    },

    Messages::class => function ($c) {
        return new Messages();
    },
    

];
