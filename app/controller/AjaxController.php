<?php
namespace App\Controller;

use DI\Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 *
 */
class AjaxController
{

    public function install(Request $request, Response $response, Container $c)
    {
        $view = $c->get(\Slim\Views\Twig::class);
        $num1 = $request->getParam('num1');
        $num2 = $request->getParam('num2');
        $ope   = $request->getParam('ope');

        $result = 0;

        switch ($ope) {
            case '+':
                $result = $num1 + $num2;
                break;
            case '-':
                $result = $num1 - $num2;
                break;

            case '*':
                $result = $num1 * $num2;
                break;

            case '/':
                $result = $num1 / $num2;
                break;
        }

        $data = [
            'num1'   => $num1,
            'num2'   => $num2,
            'ope'    => $ope,
            'result' => $result,

        ];

        echo $view->fetch('templates/ajax.twig', $data);

    }

}
