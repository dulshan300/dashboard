<?php
namespace App\Controller;

use Illuminate\Database\Capsule\Manager as Capsule;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Router;
use Slim\Views\Twig;
use DI\Container;

/**
 *
 */
class AppController
{

    public function install(Request $request, Response $response, Router $router, Twig $view, Capsule $db)
    {

    }

    public function ajaxHandler(Request $request, Response $response, Container $container)
    {
        
        $controller = $request->getParam('controller');
        $controller = explode(':', $controller);
        $method     = $controller[1];
        $obj        = '\\App\\Controller\\' . $controller[0];
        $obj        = new $obj();

        $obj->$method($request, $response, $container);

        
    }
    
}
