<?php
namespace App\Controller\Auth;

use App\Model\Auth\Auth;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Respect\Validation\Validator as v;
use Slim\Flash\Messages;
use Slim\Router;
use Slim\Views\Twig;

/**
 *
 */
class AuthController
{

    public function login(Request $request, Response $response, Twig $view, Auth $auth, Router $router)
    {
        if ($auth->check()) {
            return $response->withRedirect($router->pathFor('dashboard.home'));
        }
        return $view->render($response, 'pages/auth/login.twig');
    }

    public function postLogin(Request $request, Response $response, Router $router, Auth $auth, Messages $flash)
    {
        $validator = new \App\Validation\Validator($request, [

            'email'    => v::notEmpty()->email(),
            'password' => v::notEmpty()->Length(4, null),

        ]);

        if (!$validator->valid()) {
            $flash->addMessage('error', $validator->getFirstErrorList());
            return $response->withRedirect($router->pathFor('auth'));
        }

        $email    = $request->getParam('email');
        $password = $request->getParam('password');

        if (!$auth->attempt($email, $password)) {
            $flash->addMessage("error", "User Name or Password incorrect");
            return $response->withRedirect($router->pathFor('auth'));
        }

        return $response->withRedirect($router->pathFor('dashboard.home'));

    }

    public function signup(Request $request, Response $response, Twig $view)
    {

        return $view->render($response, 'pages/auth/signUp.twig');
    }

    public function postSignup(Request $request, Response $response, Router $router, Messages $flash)
    {

        v::with('App\\Validation\\Rules\\');

        $validator = new \App\Validation\Validator($request, [

            'userName' => v::notEmpty()->Length(3, null),
            'email'    => v::notEmpty()->email()->UniqEmail(),
            'password' => v::notEmpty()->Length(4, null),

        ]);

        if (!$validator->valid()) {
            $flash->addMessage('error', $validator->getFirstErrorList());
            return $response->withRedirect($router->pathFor('auth.signup'));
        }

        $password        = $request->getParam('password');
        $confirmPassword = $request->getParam('confirmPassword');

        if ($password != $confirmPassword) {
            $flash->addMessage('error', "Password not match");
            return $response->withRedirect($router->pathFor('auth.signup'));
        }

        \App\Model\Auth\User::create([
            'user_name' => $request->getParam('userName'),
            'email'     => $request->getParam('email'),
            'password'  => password_hash($request->getParam('password'), PASSWORD_DEFAULT),
            "hash"      => hash('ripemd160', $request->getParam('email') . $request->getParam('user_name')),
        ]);

        return $response->withRedirect($router->pathFor('auth'));
    }

    public function logout(Request $request, Response $response, Router $router)
    {
        unset($_SESSION['user']);
        return $response->withRedirect($router->pathFor('auth'));
    }
}
