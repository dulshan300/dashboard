<?php 
namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Router;
use Slim\Views\Twig;

/**
 * 
 */
class DashBoardController
{
	
	public function index(Request $request,Response $response, Router $router,Twig $view)
	{
		return $view->render($response,'pages/start.twig');
	}
}