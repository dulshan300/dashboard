<?php 

namespace App\Model\Auth;

use App\Model\Auth\User;

/**
 * 
 */
class Auth
{
	
	public function attempt($email, $password)
    {

        // grab the user
        $user = User::where('email', $email)->first();

        if (!$user) {
            return false;
        }

        if (password_verify($password, $user->password)) {
            $_SESSION['user'] = $user->id;
            return true;
        }

        return false;
    }

    public function check()
    {
        return isset($_SESSION['user']);
    }

    public function user()
    {
        return User::find($_SESSION['user']);
    }
	
}