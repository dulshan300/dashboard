<?php
namespace App\Model\Auth;

use Illuminate\Database\Eloquent\Model;
/**
 *
 */
class User extends Model
{

    protected $fillable = [
        "user_name", "email", "password", "hash", "privilege",
    ];

    public function setPassword($password)
    {
        $this->update([
            "password" => password_hash($password, PASSWORD_DEFAULT),
        ]);
    }

}
