<?php

use Phinx\Migration\AbstractMigration;

class CreateUserMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $user_tbl = $this->table("users");
        $user_tbl->addColumn("user_name", "string", ['limit' => 255])
            ->addColumn("password", "string", ['limit' => 255])
            ->addColumn("email", "string", ['limit' => 255])
            ->addColumn("hash", "string", ['limit' => 255])            
            ->addColumn("privilege", "integer", ['limit' => 3, 'null' => true, 'default' => 1])
            ->addColumn("created_at", "timestamp", ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn("updated_at", "timestamp", ['default' => 'CURRENT_TIMESTAMP'])
            ->create();
    }
}
