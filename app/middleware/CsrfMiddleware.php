<?php
namespace App\Middleware;

use Slim\Csrf\Guard;

/**
 *
 */
class CsrfMiddleware extends \App\Middleware\Middleware
{

    public function __invoke($request, $response, $next)
    {
        $csrf = $this->c->get(Guard::class);

        $csrfNameKey  = $csrf->getTokenNameKey();
        $csrfValueKey = $csrf->getTokenValueKey();
        $csrfName     = $request->getAttribute($csrfNameKey);
        $csrfValue    = $request->getAttribute($csrfValueKey);

        $html = '<input type="hidden" name="' . $csrfNameKey . '" value="' . $csrfName . '">';
        $html .= '<input type="hidden" name="' . $csrfValueKey . '" value="' . $csrfValue . '">';

        $this->c->get(\Slim\Views\Twig::class)->getEnvironment()->addGlobal('csrf', ['field' => $html]);

        $response = $next($request, $response);

        return $response;
    }
}
