<?php
namespace App\Middleware;

use App\Model\Auth\Auth;
use App\Middleware\AuthMiddleware;
use Slim\Flash\Messages;
use Slim\Router;
/**
 *
 */
class AuthMiddleware extends \App\Middleware\Middleware
{

    public function __invoke($request, $response, $next)
    {
        $auth   = $this->c->get(Auth::class);
        $router = $this->c->get(Router::class);
        $flash = $this->c->get(Messages::class);


        if (!$auth->check()) {
            $flash->addMessage('info', "Please login to Access the system dashboard");
            return $response->withRedirect($router->pathFor('auth'));

        }

        $response = $next($request, $response);
      

        return $response;
    }
}
