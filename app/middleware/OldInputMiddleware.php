<?php
namespace App\Middleware;

/**
 *
 */
class OldInputMiddleware extends \App\Middleware\Middleware
{

    public function __invoke($request, $response, $next)
    {
        // session old comming from the validation class

        if (isset($_SESSION['errors'])) {
            $this->c->get(\Slim\Views\Twig::class)->getEnvironment()->addGlobal('old', $_SESSION['old']);
        }

        unset($_SESSION['errors']);
        unset($_SESSION['old']);

        $response = $next($request, $response);

        $_SESSION['old'] = $request->getParams();

        return $response;
    }
}
