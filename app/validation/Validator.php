<?php

namespace App\Validation;

use Psr\Http\Message\ServerRequestInterface as Request;
use Respect\Validation\Exceptions\NestedValidationException;

/**
 *
 */
class Validator
{
    private $errors;

    public function __construct(Request $request, array $rules)
    {
        foreach ($rules as $key => $rule) {
            try {
                $rule->setName($key)->assert($request->getParam($key, ""));
            } catch (NestedValidationException $exception) {
                $this->errors[$key] = $exception->getMessages();
            }
        }

        $_SESSION['errors'] = $this->errors;
      
    }

    public function valid()
    {
        return empty($this->errors);
    }

    public function getFirstErrorList()
    {
        $out = "<ul>";
        foreach ($this->errors as $key => $errors) {
            $out .= "<li>" . $errors[0] . "</li>";
        }
        $out .= "</ul>";

        return $out;
    }
}
