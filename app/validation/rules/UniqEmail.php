<?php 
namespace App\Validation\Rules;

use Respect\Validation\Rules\AbstractRule;

class UniqEmail extends AbstractRule
{
    public function validate($input)
    {
        $collection = \App\Model\Auth\User::where('email',$input)->first();
        return is_null($collection);
    }
}