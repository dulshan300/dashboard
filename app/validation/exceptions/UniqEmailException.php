<?php 
namespace App\Validation\Exceptions;

use Respect\Validation\Exceptions\ValidationException;

class UniqEmailException extends ValidationException
{
    public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => '{{name}} Already Registerd.',
        ],
        self::MODE_NEGATIVE => [
            self::STANDARD => '{{name}} must not pass my rules',
        ]
    ];
}