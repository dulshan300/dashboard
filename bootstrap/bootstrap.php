<?php
/*
 * This file used to setup the application
 */

// error_reporting(E_ERROR |  E_PARSE);

require_once '../vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;

// get environment type
$env = (getenv('REMOTE_ADDR') == "::1") ? "development" : "production";

$_SERVER['env'] = $env;

// Loading env.yaml configurations
$config = \Symfony\Component\Yaml\Yaml::parseFile('../env.yaml');

// Create new App
$app = new App\App();

// Get the Container
$container = $app->getContainer();

// Add Configurations to container
$container->set('config', $config);

// Add Auth Class to Container
$container->set(Auth::class, function () {
    return new \App\Model\Auth\Auth();
});

// Add Csrf Guard class to Container
$container->set(Guard::class, function () {
    $csrf = new Slim\Csrf\Guard;
    $csrf-> setPersistentTokenMode(true);
    return $csrf;
});

// Setup Database

$capsule = new Capsule;
$capsule->addConnection($config['env_' . $env]);
$capsule->setAsGlobal();
$capsule->bootEloquent();
$container->set(Capsule::class, $capsule);

// Whoops Error Handling
$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

// Loading Routers
require_once '../app/routers.php';

// Registring Global Middleware

$app->add(new \App\Middleware\OldInputMiddleware($container));

$app->add(new \App\Middleware\CsrfMiddleware($container));

// Globle CSRF Protection
$app->add($container->get(Guard::class));
